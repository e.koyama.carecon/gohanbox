<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $user = DB::table('users')->first();

        DB::table('posts')->insert([
            [
                'user_id' => 1,
                'restaurant_id' => 'e944802',
                'date' => Carbon::now(),
                'status' => false,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'user_id' => 1,
                'restaurant_id' => 'gfnj302',
                'date' => Carbon::now(),
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            // [
            //     'user_id' => 1,
            //     'restaurant_id' => 'ya19500',
            //     'date' => Carbon::now(),
            //     'status' => true,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now(),
            // ],
            // [
            //     'user_id' => 1,
            //     'restaurant_id' => 'ya19500',
            //     'date' => Carbon::now(),
            //     'status' => true,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now(),
            // ],
            // [
            //     'user_id' => 1,
            //     'restaurant_id' => 'ya19500',
            //     'date' => Carbon::now(),
            //     'status' => false,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now(),
            // ],
            // [
            //     'user_id' => 1,
            //     'restaurant_id' => 'ya19500',
            //     'date' => Carbon::now(),
            //     'status' => true,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now(),
            // ],
            // [
            //     'user_id' => 1,
            //     'restaurant_id' => 'ya19500',
            //     'date' => Carbon::now(),
            //     'status' => true,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now(),
            // ],
            // [
            //     'user_id' => 1,
            //     'restaurant_id' => 'ya19500',
            //     'date' => Carbon::now(),
            //     'status' => false,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now(),
            // ],
        ]);

    }
}
