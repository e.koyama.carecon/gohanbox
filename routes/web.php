<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('posts/check/{restaurant_id}', 'PostController@check')->name('restaurants.check');
    Route::get('posts/uncheck/{restaurant_id}', 'PostController@uncheck')->name('restaurants.uncheck');
    Route::resource('posts', 'PostController');
// Route::view('posts', 'posts.index', ['url' => 'https://api.gnavi.co.jp/RestSearchAPI/v3/', 'keyid' => 'efa8a992882d97178436f93f2637677f']);
});

Auth::routes();
