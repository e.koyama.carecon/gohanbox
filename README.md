## About App

渋谷近辺のお店で行ったお店や行きたいお店が登録でき、自分だけのリストがつくれます。

「このお店気になるなー」と思いつつもなんとなく時が過ぎていくうちにお店の名前を忘れてしまう。
そんなときパッとリストに登録しておいたら忘れなくていいなという思いからアプリ作成に至りました。

お店のAPIはぐるなびAPIを用いて渋谷駅から半径500mで指定（現状登録ページに全て表示）

ぐるなびAPIの取得はプラグインのGuzzleを用いました。
Google maps APIはJavaScriptで実装。

## 実装環境

- Laravel5.8
- Docker
- plantUML
- Heroku（デプロイ）
- API
    - ぐるなびAPI
    - Google maps API


## 設計

### 機能一覧
- ユーザーは新規登録、ログインできる
- ログイン後、一覧ページに飛ぶ
- 一覧ページには登録したお店の一覧が出る
- 一覧ページから新規登録ページ、詳細ページへ飛ぶことができる
- 登録ページでは飲食店を検索、行った日付、感想が登録できる（現状全てはできていない）
- 登録すると一覧ページに戻る
- 詳細ページではGoogle mapでお店の位置を見ることができる

### URL設計
|URL|メソッド|処理|
|--|--|--|
|posts|GET|登録一覧ページを表示|
|posts/{post_id}|GET|詳細ページを表示|
|posts/create|GET|新規登録ページを表示|
|posts/check|POST|行ったお店の登録処理|
|posts/uncheck|POST|行きたいお店の登録処理|
|posts/{post_id}/edit|GET|お店情報の編集ページ表示|
|posts/{post_id}/update|POST|お店情報の編集処理|
|posts/{post_id}/destroy|DELETE|お店の登録削除|

### データベース設計
[plantUMLに記載](https://gitlab.com/e.koyama.carecon/gohanbox/blob/feature/koyama/plantUML/database.puml)

## 今後の課題
- 詳細ページ
    - ページネーションで20件ずつ表示
    - コメントの追加ができるようにする
    - 画像がない場合表示させない
    - キーワード検索
    - 評価高い順
    - 現在地から○mでお店を表示
    - SNS機能
    - もうちょっとマシなデザインにしたい
