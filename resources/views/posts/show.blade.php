@extends('layouts.app')
<style>
    li {
        list-style: none;
    }
    #googleMap {
    width: 100%;
    height: 400px;
    }
</style>
@section('content')

<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-header">
                <ul class="d-flex m-0 p-0">
                    <li class="flex-grow-1">{{ $datum['rest'][0]['name'] }}</li>
                    <li>
                        <a href="{{ route('posts.edit', ['id' => $post->restaurant_id]) }}">
                            <button type="button" class="btn btn-outline-secondary btn-sm mr-3">Edit</button>
                        </a>
                    </li>
                    <li>
                        <form action="{{ route('posts.destroy', ['id' => $post->restaurant_id]) }}" method="post" id="js_delete">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm" onClick="deletePost(); return false;">
                        </form>
                    </li>
                </ul>
            </div>

            <div class="card-body">
                <h5 class="card-title"></h5>
                <ul class="card-text pl-0">
                    <li>エリア：{{ $datum['rest'][0]['code']['areaname_s'] }}</li>
                    <li>ステータス：{{ $post->status_label }}</li>
                    @if($post->status == 1)
                        <li>行った日：{{ $post->formatted_date }}</li>
                    @endif
                    <li>コメント：
                        @if(isset($comment))
                            {{ $comment->content }}
                        @endif
                    </li>
                    <li class="mb-3">住所：{{ $datum['rest'][0]['address'] }}</li>
                    <li><div id="googleMap"></div></li>
                    <li class="mt-3">
                        <img src="{{ $datum['rest'][0]['image_url']['shop_image1'] }}">
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6 offset-md-3 mt-2">
        <a href="{{ route('posts.index') }}">back</a>
    </div>
</div>

@endsection

@section('scripts')
<script>
let lat = <?php echo $datum['rest'][0]['latitude']; ?>;
let lng = <?php echo $datum['rest'][0]['longitude']; ?>;

function initMap() {
    map = new google.maps.Map(document.getElementById('googleMap'), {
        center: { // 地図の中心を指定
            lat: lat, // 緯度
            lng: lng // 経度
        },
        zoom: 19 // 地図のズームを指定
    });

    latlng = new google.maps.LatLng(lat, lng);

    marker = new google.maps.Marker({
        position: latlng,
        map: map
    });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoYJq-xnKxQ-fLJf-2X0G9sZYEih7bbkk&callback=initMap" async defer></script>

<script>
function deletePost(e) {
    if(confirm('本当に削除しますか？')) {
        document.getElementById('js_delete').submit();
    }
}
</script>
