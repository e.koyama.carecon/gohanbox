@extends('layouts.app')
<style>
#post-want,
#post-done {
    display: none;
}
.border {
    width: 430px;
    border-radius: 4px;
    margin: 12px;
}
.done-title {
    padding: 12px;
    background: #6cb2eb;
}
.want-title {
    padding: 12px;
    background: #ffc107;
}

.post-body {
    padding: 12px;
    list-style: none;
}
</style>

@section('content')
    <div class="container">

    <div class="d-flex justify-content-between">
        <h1>My Restaurant's list</h1>
        <a href="{{ route('posts.create') }}">
            <button type="button" class="btn btn-outline-danger btn-lg">お店を登録する</button>
        </a>
    </div>

    <div class="d-flex justify-content-start">
        <button type="button" id="js_list-done" class="btn btn-info mx-2">行った</button>
        <button type="button" id="js_list-want" class="btn btn-warning mx-2">行きたい</button>
    </div>

    <div class="d-flex flex-wrap justify-content-start mt-5">
        @foreach($data as $datum)
        @php
            $post = Auth::user()->posts->where('restaurant_id', $datum['rest'][0]['id'])->first();
            $comment = Auth::user()->comments->where('restaurant_id', $datum['rest'][0]['id'])->first();
        @endphp

        @if($post->status == true)
        <div class="border border-primary">
            <div class="post-title done-title">
                <a href="{{ route('posts.show', ['id' => $datum['rest'][0]['id']]) }}" class="text-light">{{ $datum['rest'][0]['name'] }}</a>
            </div>
        @else
        <div class="border border-warning">
            <div class="post-title want-title">
                <a href="{{ route('posts.show', ['id' => $datum['rest'][0]['id']]) }}" class="text-muted">{{ $datum['rest'][0]['name'] }}</a>
            </div>
        @endif
            <ul class="post-body">
                <li>エリア：{{ $datum['rest'][0]['code']['areaname_s'] }}</li>
                @if($post->status == true)
                <li>行った日：{{ $post->formatted_date }}</li>
                @endif
            </ul>
        </div>
        @endforeach
    </div>

    @if(Auth::check())
    <script>
        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('logout-form').submit();
        });
    </script>
    @endif
@endsection
