@extends('layouts.app')

@section('styles')
    @include('share.flatpickr.styles')
    <style>
    /* #date {
        width: 90px;
    } */
    li {
        list-style: none;
    }
    </style>
@endsection


@section('content')
<div class="container">
    <h1>渋谷のお店一覧</h1>

    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $message)
            <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col" class="text-center">名前</th>
                    <th scope="col" class="text-center">エリア</th>
                    <th scope="col" class="text-center">お店の登録</th>
                </tr>
            </thead>
            <tbody>
                @foreach($restaurants as $restaurant)
                <tr>
                    <td>
                        <ul class="d-flex align-items-center">
                            <li><img src="{{ $restaurant['image_url']['shop_image1'] }}" width="120" height="120"></li>
                            <li class="ml-3">{{ $restaurant['name'] }}</li>
                        </ul>
                    </td>
                    <td>{{ $restaurant['code']['areaname_s'] }}</td>
                    <td>
                        @if(Auth::user()->posts->where('restaurant_id', $restaurant['id'])->first())
                        <button type="button" class="btn btn-secondary" disabled>登録済み</button>
                        @else
                        <form action="{{ route('restaurants.check', [ 'restaurant_id' => $restaurant['id'] ]) }}" method="GET">
                            {{ csrf_field() }}
                            <input type="text" class="form-control mb-1" placeholder="コメントを入力" name="comment">
                            <div class="d-flex align-items-center mb-3">
                                <input type="text" id="date" class="form-control" placeholder="日付" name="date">
                                <input type="submit" value="行ったお店として登録" class="btn btn-primary mx-2">
                            </div>
                        </form>
                        <form action="{{ route('restaurants.uncheck', [ 'restaurant_id' => $restaurant['id'] ]) }}" method="GET">
                            {{ csrf_field() }}
                            <input type="submit" value="行きたいお店として登録" class="btn btn-warning w-100">
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('posts.index') }}"><button type="button" class="btn btn-secondary">Back</button></a>


    </div>
    </div>
</div>
@endsection

@section('scripts')

<!-- 日本語化のための追加スクリプト -->
{{-- <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ja.js"></script> --}}
<script>
    const flatpickr = require('flatpickr');
    flatpickr('#date', {
        locale: 'ja',
        dateFormat: 'Y/m/d',
        minDate: 2019/1/1,
        allowInput: true
    });
</script>
    {{-- @include('share.flatpickr.scripts') --}}
{{--
    <script>
    $(function() {
        // controllerから現在のページを取得
        let page = {{ $page }};

        $.getJSON('/')

    })

    </script> --}}
@endsection
