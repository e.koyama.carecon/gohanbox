@extends('layouts.app')

@section('styles')
    @include('share.flatpickr.styles')
    <style>
    li {
        list-style: none;
    }
    </style>
@endsection


@section('content')
<div class="container">
    <h1>Edit</h1>

    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $message)
            <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
    <div class="col-md-6 offset-md-3">

    <form action="{{ route('posts.update', ['id' => $post->restaurant_id]) }}" method="post">
    @csrf
    {{ method_field('patch') }}
        <div class="form-group">{{ $datum['rest'][0]['name'] }}</div>
        <div class="form-group d-flex">
            <label><input type="radio" id="js-check" class="form-control" name="status" value="1" {{ old('status', $post->status) === 1 ? 'checked': '' }} onClick="buttonCheck();">行った</label>
            <label><input type="radio" id="js-uncheck" class="form-control" name="status" value="0" {{ old('status', $post->status) === 0 ? 'checked': '' }} onClick="buttonUnCheck();">行きたい</label>
        </div>
        @if($post->status === 1)
        <div class="form-group" id="js-dateForm">
            <label for="date">日付</label>
            <input type="text" class="form-control" name="date" id="date" value="{{ old('date', $post->formatted_date) }}">
        </div>
        @else
        <div class="form-group" id="js-dateForm" style="display:none;">
            <label for="date">日付</label>
            <input type="text" class="form-control" name="date" id="date" value="{{ old('date', $post->formatted_date) }}">
        </div>
        @endif

        <a href="{{ route('posts.show', ['restaurant_id' => $post->restaurant_id]) }}"><button type="button" class="btn btn-secondary">Back</button></a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    </div>
    </div>
</div>
@endsection

@section('scripts')
    @include('share.flatpickr.scripts')
    <script>
        function buttonCheck() {
            document.getElementById('js-dateForm').style.display = 'block';
        }
        function buttonUnCheck() {
            document.getElementById('js-dateForm').style.display = 'none';
        }
    </script>
@endsection
