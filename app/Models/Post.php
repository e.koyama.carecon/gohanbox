<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Post extends Model
{
    protected $table = 'posts';

    // statusにラベルをつける
    const STATUS = [
        0 => ['label' => '行きたい'],
        1 => ['label' => '行った'],
    ];
    public function getStatusLabelAttribute()
    {
        $status = $this->attributes['status'];

        if(!isset(self::STATUS[$status])){
            return '';
        }
        return self::STATUS[$status]['label'];
    }

    // 日付の形式も変える
    public function getFormattedDateAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->attributes['date'])->format('Y/m/d');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getAllPosts()
    {
        return Post::all();
    }

    public function getUserPosts(int $user_id)
    {
        $posts = Post::where('user_id', $user_id)->get();
        return $posts;
    }

    public function createPostCheck(string $restaurant_id, $date, $comment_text)
    {
        $post = new Post();
        $comment = new Comment();
        // dd($comment);

        // ポストテーブルの保存
        $post->restaurant_id = $restaurant_id;
        $post->user_id = Auth::user()->id;
        if ($date == null) {
            $post->date = Carbon::now()->format('Y/m/d');
        } else {
            $post->date = $date;
        }
        $post->status = true;
        Auth::user()->posts()->save($post);

        // コメントテーブルの保存
        $comment->restaurant_id = $post->restaurant_id;
        $comment->user_id = $post->user_id;
        $comment->content = $comment_text;
        Auth::user()->comments()->save($comment);

        return $post;
    }

    public function createPostUncheck(string $restaurant_id)
    {
        $post = new Post();

        $post->restaurant_id = $restaurant_id;
        $post->user_id = Auth::user()->id;
        $post->date = Carbon::now()->format('Y/m/d');

        $post->status = false;
        Auth::user()->posts()->save($post);

        return $post;
    }

    public function editPost(Post $post, Request $request)
    {
        $post->date = $request->date;
        $post->status = $request->status;
        $post->save();
    }

    public function getPost($id)
    {
        return Post::where('restaurant_id', $id)->first();
    }
}
