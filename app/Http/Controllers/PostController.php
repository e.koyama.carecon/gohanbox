<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PostService;
use App\Services\CommentService;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\DB;
use App\Http\Requests\CreatePost;
use App\Http\Requests\EditPost;
use Prophecy\Call\Call;

class PostController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService, CommentService $commentService)
    {
        $this->postService = $postService;
        $this->commentService = $commentService;
    }

    public function index()
    {
        // ユーザーの持つポスト取得
        $post_ids = Auth::user()->posts()->pluck('restaurant_id');
        foreach($post_ids as $post_id)
        {
            $data[] = $this->postService->getAPIData(['id' => $post_id]);
            $post[] = Auth::user()->posts->where('restaurant_id', $post_id)->first();
        }
        return view('posts.index', [
            'post' => $post,
            'data' => $data,
        ]);
    }

    public function show(string $restaurant_id)
    {
        $post = $this->postService->getPost($restaurant_id);
        $datum = $this->postService->getAPIData(['id' => $restaurant_id]);
        $comment = $this->commentService->getComment($restaurant_id);

        return view('posts.show', [
            'post'    => $post,
            'datum'   => $datum,
            'comment' => $comment
        ]);
    }

    public function create()
    {
        $res = $this->postService->getAPIData();

        return view('posts/create', [
            'restaurants' => $res['rest'],
        ]);
    }

    public function check(string $restaurant_id, Request $request)
    {
        $date = $request->date;
        $comment_text = $request->comment;
        $post = $this->postService->createPostCheck($restaurant_id, $date, $comment_text);

        return redirect()->route('posts.index');
    }

    public function uncheck(string $restaurant_id)
    {
        $post = $this->postService->createPostUncheck($restaurant_id);

        return redirect()->route('posts.index');
    }

    public function edit(string $restaurant_id)
    {
        $post = $this->postService->getPost($restaurant_id);
        $datum = $this->postService->getAPIData(['id' => $restaurant_id]);

        return view('posts/edit', [
            'post' => $post,
            'datum' => $datum,
        ]);
    }

    public function update(Request $request, string $restaurant_id)
    {
        $post = $this->postService->getPost($restaurant_id);
        $this->postService->editPost($post, $request);

        return redirect()->route('posts.index');
    }

    public function destroy(string $restaurant_id)
    {
        $post = $this->postService->getPost($restaurant_id);

        $post->delete();

        return redirect()->to('posts');
    }
}
