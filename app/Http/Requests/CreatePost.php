<?php

namespace App\Http\Requests;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class CreatePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * リクエストに基づいた権限チェック trueはリクエストを受け付ける
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'restaurant_id' => 'お店の名前',
            'date' => '日付',
            'status' => '行ったor行きたい',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     * 入力欄のチェックはここに書く
     *
     * @return array
     */
    public function rules()
    {
        return [
            'restaurant_id' => 'required|max:100',
            'status' => 'required|boolean',
        ];
    }
}
