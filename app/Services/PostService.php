<?php

namespace App\Services;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class PostService extends Service
{
    // モデルから受け取るインスタンスを入れる箱を用意
    private $postModel;

    // インスタンスを生成
    public function __construct(Post $postModel)
    {
        $this->postModel = $postModel;
    }

    /**
    *  DBから全フォルダー情報を取得
    *  @param Postモデル
    *  @return Postコレクション
    */
    public function getAllPosts()
    {
        return $this->postModel->getAllPosts();
    }

    public function getApiData(array $param = [])
    {
        $client = new \GuzzleHttp\Client();
        $path = 'https://api.gnavi.co.jp/RestSearchAPI/v3/';
        $response = $client->get(
            $path,
            [
                'query' => [
                    'keyid' => 'efa8a992882d97178436f93f2637677f',
                    'areacode_m' => 'AREAM2126',
                    'hit_per_page' => 100,
                    'id' => $param['id'] ?? "",
                    // '' => $param['']
                    //'' => $param['']
                    //'' => $param['']
                ],
            ]
        );
        $response_body_json = (string) $response->getBody();
        $response_body = json_decode($response_body_json, true);
        return $response_body;
    }

    /**
    *  DBからユーザーごとのフォルダー情報を取得
    *  @param Postモデル
    *  @return Postコレクション
    */
    public function getUserPosts(int $id)
    {
        return $this->postModel->getUserPosts($id);
    }

    public function createPostCheck(string $restaurant_id, $date, $comment_text)
    {
        return $this->postModel->createPostCheck($restaurant_id, $date, $comment_text);
    }

    public function createPostUnCheck(string $restaurant_id)
    {
        return $this->postModel->createPostUncheck($restaurant_id);
    }

    public function editPost(Post $post, Request $request)
    {
        $this->postModel->editPost($post, $request);
    }

    public function getPost(string $id)
    {
        return $this->postModel->getPost($id);
    }
}
