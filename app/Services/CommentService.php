<?php

namespace App\Services;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;

class CommentService extends Service
{
  // モデルから受け取るインスタンスを入れる箱を用意
    private $commentModel;

    // インスタンスを生成
    public function __construct(Comment $commentModel)
    {
        $this->commentModel = $commentModel;
    }

    public function getComment(string $id)
    {
        return $this->commentModel->comment($id);
    }
}
